(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
var main = document.getElementById('main-container');
var dataStorage = localStorage.getItem('session_auth');
//html templates

var homePage = `
	<div class="loginpage">
		<img src="./assets/img/logo-jetty.png" alt="Buen Viaje. Siempre" class="img-responsive center-block" width="50%"/>
		<p class="text-center slogan">Buen Viaje. Siempre.</p>
		<form name="loginForm" id="loginForm">
		    <fieldset>
		    	<div class="form-group">
		        	<input class="form-control input-lg" type="text" name="email" placeholder="Correo electrónico">
		        </div>
		        <div class="form-group">
		        	<input class="form-control input-lg" type="password" name="password" placeholder="Contraseña">
		        </div>
		        <div class="form-group">
		        	<button class="btn btn-primary btn-lg btn-block" type="submit" name="submitLogin" id="submitLogin" >Iniciar sesión</button>
		        </div>
		    </fieldset>
    	<p id="msg"></p>
		</form>
	</div>`;

var dashboardPage = `
	<nav class="navbar navbar-default">
	      <div class="navbar-header">
	        <button type="button" class="pull-left navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	          <span class="sr-only">Toggle navigation</span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	          <span class="icon-bar"></span>
	        </button>
	      </div>
	      <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false" style="height: 0px;">
	        <ul class="nav navbar-nav navbar-right">
	          <li><a href="/">Cerrar sesión</a></li>
	        </ul>
	      </div>
	</nav>
	<p class="text-center">Corridas asignadas</p>
	<hr>
	<section>
		<article class="col-md-4 com-xs-12">
			<h3></h3>
		</article>
	</section>
`;

//end html templates

function checkIndex(dataStorage) {
	if (dataStorage === null) {
		main.innerHTML = "";
		main.innerHTML = homePage;
	} else {
		main.innerHTML = "";
		main.innerHTML = dashboardPage;
	}
}

checkIndex(dataStorage);

document.addEventListener('DOMContentLoaded', function () {
	document.getElementById('submitLogin').addEventListener('click', function (e) {
		console.log("submit login form");
		e.preventDefault();
		var data = {
			driver: {
				email: document.forms["loginForm"]["email"].value,
				password: document.forms["loginForm"]["password"].value
			}
		};
		var Url = "https://jettymx-st.herokuapp.com/api/drivers/session";
		var xhr = new XMLHttpRequest();
		xhr.open('POST', Url, true);
		xhr.setRequestHeader("Content-type", "application/json");
		xhr.send(JSON.stringify(data));
		xhr.onreadystatechange = function () {
			document.getElementById("msg").innerHTML = "";
			if (xhr.readyState === 4) {
				var response = JSON.parse(xhr.responseText);
				//console.log(response);
				if (xhr.status == 200) {
					// success
					index = 1;
					console.log(response.auth_token);
					var sendData = { 'Authorization': 'Token ' + response.auth_token + ',email=' + response.email };
					//{'Authorization': 'Token '+this.token +',email='+this.email}
					localStorage.setItem('session_auth', response.auth_token);
					dataStorage = localStorage.getItem('session_auth');
					req = new XMLHttpRequest();
					req.open('GET', 'https://jettymx-st.herokuapp.com/api/drivers/trips', true);
					req.setRequestHeader("Authorization", "Token " + response.auth_token + ",email=" + response.email);
					req.send();
					req.onreadystatechange = function () {
						if (req.readyState === 4) {
							var res = req.responseText;
							console.log(res);
							if (req.status == 200) {
								checkIndex(dataStorage);
								console.log(res);
							} else if (req.status >= 500) {
								// internal server error
								console.log(res);
							} else if (req.status >= 402 && req.status <= 420) {
								// error
								console.log(res);
							} else if (req.status == 400 || req.status == 401) {
								// bad request & unauthorized error
								console.log(res);
							}
						}
					};
					//document.getElementById("msg").innerHTML = "ok";
				} else if (xhr.status >= 500) {
					// internal server error
				} else if (xhr.status >= 402 && xhr.status <= 420) {
					// error
				} else if (xhr.status == 400 || xhr.status == 401) {
					// bad request & unauthorized error
					document.getElementById("msg").innerHTML = `<p class="text-danger">${response.message}</p>`;
				}
			}
		};
	}, false);
});

},{}]},{},[1]);
