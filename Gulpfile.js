var gulp = require('gulp');
var babel = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('scripts', function(){
	browserify('./src/app.js')
		.transform(babel)
		.bundle()
		.pipe(source('app.js'))
		.pipe(gulp.dest('assets/js'));
});

gulp.task('default',['scripts']);